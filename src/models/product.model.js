
const productSchema = {
    name : String,
    subname : String,
    price : Number
}

module.exports = productSchema;
