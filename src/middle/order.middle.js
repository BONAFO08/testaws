const validate = require('../controllers/dataVerify');
const orderControl = require('../controllers/order.controller');
const userControl = require('../controllers/user.controller');
const db = require('../config/conexion.config');


//Create a new order
const newOrder = async (req, res) => {
    let desToken = userControl.validateAdmin(req.headers.authorization);
    if (desToken != false) {
        let selectedFood = validate.cleaningFood(req.body.arrFood);
        selectedFood = validate.validateFood(selectedFood);
        let selectedAddress = validate.validateAddress(req.body.address);
        let idPay = { id: req.query.idPay };
        idPay = validate.validatePayData(idPay, '', 'update');

        if (selectedFood != false && idPay.id != false && selectedAddress != false) {
            let response = await orderControl.createOrder(desToken, selectedAddress, selectedFood, idPay.id);
            res.status(response.status).send(response.txt);
        } else {
            res.status(400).send('Lo siento.Haz enviado datos invalidos.');
        }
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}

//Modify an order
const modOrder = async (req, res) => {
    let desToken = userControl.validateAdmin(req.headers.authorization);
    if (desToken != false) {
        let selectedFood = validate.cleaningFood(req.body.arrFood);
        selectedFood = validate.validateFood(selectedFood);
        let selectedAddress = validate.validateAddress(req.body.address);
        let idPay = { id: req.query.idPay };
        idPay = validate.validatePayData(idPay, '', 'update');
        let idOrder = { id: req.query.idOrder };
        idOrder = validate.validatePayData(idOrder, '', 'update');

        if ((selectedFood != false || idPay.id != false || selectedAddress != false) && idOrder.id != false) {
            let response = await orderControl.modifyOrder(desToken, selectedAddress, selectedFood, idPay.id, idOrder.id);
            res.status(response.status).send(response.txt);
        } else {
            res.status(400).send('Lo siento.Haz enviado datos invalidos.');
        }
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}

//Cancel an order
const cancelOrder = async (req, res) => {
    let desToken = userControl.validateAdmin(req.headers.authorization);
    if (desToken != false) {
        let idOrder = { id: req.query.idOrder };
        idOrder = validate.validatePayData(idOrder, '', 'update');

        if (idOrder.id != false) {
            let response = await orderControl.cancelateOrder(desToken, idOrder.id);
            res.status(response.status).send(response.txt);
        } else {
            res.status(400).send('Lo siento.Haz enviado datos invalidos.');
        }
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}

//Confirm an order
const confOrder = async (req, res) => {
    let desToken = userControl.validateAdmin(req.headers.authorization);
    if (desToken != false) {
        let idOrder = { id: req.query.idOrder };
        idOrder = validate.validatePayData(idOrder, '', 'update');

        if (idOrder.id != false) {
            let response = await orderControl.confirmOrder(desToken, idOrder.id);
            res.status(response.status).send(response.txt);
        } else {
            res.status(400).send('Lo siento.Haz enviado datos invalidos.');
        }
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}

//Change the stade of an order
const changeStadeOrder = async (req, res) => {
    let desToken = userControl.validateAdmin(req.headers.authorization);
    if (desToken != false) {
        if (desToken.range == 'admin') {
            let idOrder = { id: req.query.idOrder };
            idOrder = validate.validatePayData(idOrder, '', 'update');
            let validator = validate.validateOrderStade(req.query.newStade);
            if (idOrder.id != false && validator != false) {
                let response = await orderControl.ordenStade(idOrder.id, req.query.newStade);
                res.status(response.status).send(response.txt);
            } else {
                res.status(400).send('Lo siento.Haz enviado datos invalidos.');
            }
        } else {
            res.status(403).send('Lo siento, no tienes permiso para acceder a este contenido.');
        }
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}

//Show all orders of a user
const showUserOrders = async (req, res) => {
    let desToken = userControl.validateAdmin(req.headers.authorization);
    if (desToken != false) {
       let response = await orderControl.userOrders(desToken);
       (response.length == 0) ? (response = 'Aun no has echo ningun pedido') : ('');
       res.status(200).send(response);
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}

//Show all orders
const showAllOrders = (req, res) => {
    let desToken = userControl.validateAdmin(req.headers.authorization);
    if (desToken != false) {
        if (desToken.range == 'admin') {
            db.Order.find()
                .then(resolve => res.status(200).send(resolve))
                .catch(err => res.status(404).send(err));
        } else {
            res.status(403).send('Lo siento, no tienes permiso para acceder a este contenido.');
        }
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}


module.exports = {
    newOrder,
    modOrder,
    cancelOrder,
    confOrder,
    changeStadeOrder,
    showUserOrders,
    showAllOrders
};