const client = require('../config/redis.config');
const db = require('../config/conexion.config');


//Show all the cache 'products' 
const cacheProduct = async () => {
    const result = await client.redisClient.getAsync('products');
    return result
}


//Save data in cache 'products'
const saveInCacheProd = (cache) => {
    client.redisClient.set('products', JSON.stringify(cache), 'EX', 60*60*4);
}

//Send the actual data in the cache "products" and send a boolean with the status of this 
const consumeCache = async () => {
    let msj = {
        boolean: true,
        txt: ''
    };
    let response = await cacheProduct()
        .then(res => (res == null) ? (msj.boolean = false, msj.txt = null) : (msj.boolean = true, msj.txt = res))
        .catch(err => { msj.boolean = false; msj.txt = null })
    return msj
}

//Send the actual data in the cache "products" or if it's null
//Get it from database ,send it, and save it in the cache
const showCacheProducts = async () => {
    let msj;
    let response = await consumeCache().then(res => msj = res);
    let arrProducts;
    (msj.txt == null)

        ? (console.log('bd'),
            msj.txt = await db.Product.find(),
            saveInCacheProd(msj.txt),
            arrProducts = msj.txt)

        : (console.log('cache'), 
        arrProducts = JSON.parse(msj.txt));
    return arrProducts;
}

//Save the new product in the cache
const newCacheProduct = async (data) => {
    let msj;
    let response = await consumeCache().then(res => msj = res);
    let arrProducts = JSON.parse(msj.txt);
    if(arrProducts != null){
        arrProducts.push(data);
        saveInCacheProd(arrProducts);
    }
}

//Delete a product in the cache
const deleteACacheProduct = async (id) => {
    let msj;
    let response = await consumeCache().then(res => msj = res);
    let arrProducts = JSON.parse(msj.txt);
    let arrFilter;
    if(arrProducts != null){
        arrFilter = arrProducts.filter(arrProducts => arrProducts._id != id);
        saveInCacheProd(arrFilter)
    }
}

//Modify a product in the cache
const modifyACacheProduct = async (data) => {
    let msj;
    let response = await consumeCache().then(res => msj = res);
    let arrProducts = JSON.parse(msj.txt);

    if(arrProducts != null){
        let productIndex;
        productIndex = arrProducts.filter(arrProducts => arrProducts._id == data._id.toString());
        productIndex = arrProducts.indexOf(productIndex[0]);
        arrProducts[productIndex] = data; 
        saveInCacheProd(arrProducts);
    }
}



module.exports = {
    showCacheProducts,
    newCacheProduct,
    deleteACacheProduct,
    modifyACacheProduct,
    cacheProduct
};