const payControl = require('../controllers/pay.controller')
const validate = require('../controllers/dataVerify');
const userControl = require('../controllers/user.controller')

//Create a new payment
const newPay = (req, res) => {
    let desToken = userControl.validateAdmin(req.headers.authorization);
    if (desToken != false) {
        if (desToken.range == 'admin') {
            let pay = validate.validatePayData(req.body, '', 'new')
            if (pay.boolean == true) {
                payControl.createPay(pay.data)
                    .then(resolve => res.status(200).send(resolve))
                    .catch(err => res.status(409).send(err));
            } else {
                res.status(400).send('Lo siento.Haz enviado datos invalidos.');
            }
        } else {
            res.status(403).send('Lo siento, no tienes permiso para acceder este contenido.');
        }
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}

//Modify a payment
const modPay = async (req, res) => {

    let desToken = userControl.validateAdmin(req.headers.authorization);

    if (desToken != false) {
        if (desToken.range == 'admin') {
            req.body['id'] = req.query.id;  
            let pay = validate.validatePayData(req.body, '', 'update');
            if (pay.name != false && pay.id != false) {
            let response = await payControl.modifyAPay(pay, pay.id);
            res.status(response.status).send(response.txt);
            }else{
                res.status(400).send('Lo siento.Haz enviado datos invalidos.');
            }
        } else {
            res.status(403).send('Lo siento, no tienes permiso para acceder a este contenido.');
        }
    } else {
        res.status(403).send('Token invalido o expirado');
    }
};

//Delete a payment
const delPay = async (req, res) => {

    let desToken = userControl.validateAdmin(req.headers.authorization);

    if (desToken != false) {
        if (desToken.range == 'admin') {
            let pay = validate.validatePayData(req.query, '', 'update');
            if (pay.id != false) {
                let response = await payControl.deletePay(pay.id);
                res.status(response.status).send(response.txt);
            }else{
                res.status(400).send('Lo siento.Haz enviado datos invalidos.');
            }
        } else {
            res.status(403).send('Lo siento, no tienes permiso para acceder a este contenido.');
        }
    } else {
        res.status(403).send('Token invalido o expirado');
    }
};
module.exports = {
    newPay,
    modPay,
    delPay
}