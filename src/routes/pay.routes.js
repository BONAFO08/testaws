const express = require('express');
const router = express.Router();
const db = require('../config/conexion.config');
const payMiddle = require('../middle/pay.middle');
const userMiddle = require('../middle/user.middle');

/**
 * @swagger
 * /user/payments:
 *  get:
 *    tags: 
 *      [Pay]
 *    summary: Mostrar medios de pagos 
 *    description: Muestra todos los medios de pagos
 *    parameters:
 *    - name: authorization
 *      description: Token de usuario
 *      in: header
 *      required: false
 *      type: string
 *    responses:
 *            200:
 *                description: Se muestran todos los medios de pago
 *            403:
 *                description: Credenciales incorrectas
 */

router.get('/user/payments', (req, res) => {
    userMiddle.showDB(req, res, db.Pay);
});

/**
 * @swagger
 * /admin/newPay:
 *  post:
 *    tags: 
 *      [Pay]
 *    summary: Crear Medio de Pago
 *    description: Crea un medio de pago y lo almacena en la Base de Datos
 *    parameters:
 *    - name: authorization
 *      description: Token de usuario
 *      in: header
 *      required: false
 *      type: string
 *    - name: name
 *      description: Nombre del medios de pago
 *      in: formData
 *      required: false
 *      type: string
 *    responses:
 *            200:
 *                description: Medio de pago creado exitosamente
 *            403:
 *                description: Credenciales incorrectas
 *            400:
 *                description: Error al validar los datos 
 *            409:
 *                description: El nombre del medio de pago ya existe
 */

router.post('/admin/newPay', (req, res) => {
    payMiddle.newPay(req, res);
});


/**
 * @swagger
 * /admin/modPay/:
 *  put:
 *    tags: 
 *      [Pay]
 *    summary: Modificar datos de un Medio de Pago
 *    description: Modifica los datos de un medio de pago
 *    parameters:
 *    - name: authorization
 *      description: Token de admin
 *      in: header
 *      required: false
 *      type: string
 *    - name: id
 *      description: Id del medio de pago a modificar
 *      in: query
 *      required: false
 *      type: string
 *    - name: name
 *      description: Nombre del medio de pago
 *      in: formData
 *      required: false
 *      type: string
 *    responses:
 *            200:
 *                description: Medio de pago modificado exitosamente
 *            403:
 *                description: Credenciales incorrectas
 *            404:
 *                description: Medio de pago no encontrado
 *            400:
 *                description: Error al validar los datos 
 *            409:
 *                description: El nombre del medio de pago ya existe
 * 
 */


router.put("/admin/modPay/", (req, res) => {
    payMiddle.modPay(req, res);
});

/**
 * @swagger
 * /admin/delPay/:
 *  delete:
 *    tags: 
 *      [Pay]
 *    summary: Eliminar Medio de pago
 *    description: Elimina un medio de pago de la Base de Datos
 *    parameters:
 *    - name: authorization
 *      description: Token de admin
 *      in: header
 *      required: false
 *      type: string
 *    - name: id
 *      description: Id del medio de pago a eliminar
 *      in: query
 *      required: false
 *      type: string
 *    responses:
 *            200:
 *                description: Medio de pago eliminado exitosamente
 *            403:
 *                description: Credenciales incorrectas
 *            404:
 *                description: Medio de pago no encontrado
 *            400:
 *                description: Error al validar los datos 
 * 
 */


router.delete("/admin/delPay/", (req, res) => {
    payMiddle.delPay(req, res);
});




module.exports = router;