require('dotenv').config();
const mongoose = require('mongoose');
const userSchema = require('../models/user.model');
const productSchema = require('../models/product.model');
const paySchema = require('../models/pay.model');
const orderSchema = require('../models/order.model');

const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}

// ENV
const MONGODB_HOST = process.env.MONGODB_HOST;
const MONGODB_PORT = process.env.MONGODB_PORT;
const MONGODB_MOVIE_DB_NAME = process.env.MONGODB_MOVIE_DB_NAME;

const conectionString = `mongodb://${MONGODB_HOST}:${MONGODB_PORT}/${MONGODB_MOVIE_DB_NAME}`;


mongoose.connect(conectionString, options);


//MODELS
const User = mongoose.model('users', userSchema);
const Product = mongoose.model('products', productSchema);
const Pay = mongoose.model('payments', paySchema);
const Order = mongoose.model('orders', orderSchema);

module.exports = {
    mongoose,
    User,
    Product,
    Pay,
    Order,

};