const productControl = require('../controllers/product.controller')
const validate = require('../controllers/dataVerify');
const userControl = require('../controllers/user.controller')
const productRedis = require('../controllers/redis.product');

//Show all products (in database and cache)
const showProductDB = (req, res) => {
    let desToken = userControl.validateAdmin(req.headers.authorization);
    if (desToken != false) {
        productRedis.showCacheProducts().
            then(resolve => res.status(200).json(resolve))
            .catch(err => res.status(400).send(err));
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}

//Create a new product
const newProduct = (req, res) => {
    let desToken = userControl.validateAdmin(req.headers.authorization);
    if (desToken != false) {
        if (desToken.range == 'admin') {
            let product = validate.validateProductData(req.body, '', 'new')
            if (product.boolean == true) {
                productControl.createProduct(product.data)
                    .then(resolve => res.status(200).send(resolve))
                    .catch(err => res.status(409).send(err));
            } else {
                res.status(400).send('Lo siento.Has enviado datos invalidos.');
            }
        } else {
            res.status(403).send('Lo siento, no tienes permiso para acceder a este contenido.');
        }
    } else {
        res.status(403).send('Token invalido o expirado');
    }

}


//Modify a product
const modProduct = async (req, res) => {

    let desToken = userControl.validateAdmin(req.headers.authorization);

    if (desToken != false) {
        if (desToken.range == 'admin') {
            req.body['id'] = req.query.id;  
            let product = validate.validateProductData(req.body, '', 'update')
            let validator = product.name || product.subname || product.price;
            let validator2 = product.id;
            if (validator != false && validator2 != false) {
                let response = await productControl.modifyAProduct(product, product.id);
                res.status(response.status).send(response.txt)
            } else {
                res.status(400).send('Lo siento.Has enviado datos invalidos.');
            }
        } else {
            res.status(403).send('Lo siento, no tienes permiso para acceder a este contenido.');
        }
    } else {
        res.status(403).send('Token invalido o expirado');
    }
};

//Delete a product
const delProduct = async (req, res) => {

    let desToken = userControl.validateAdmin(req.headers.authorization);

    if (desToken != false) {
        if (desToken.range == 'admin') {    
            let product = validate.validateProductData(req.query, '', 'update');
            let validator = product.id;
            if (validator != false) {
                let response = await productControl.deleteProduct(product.id);
                res.status(response.status).send(response.txt);
            } else {
                res.status(400).send('Lo siento.Has enviado datos invalidos.');
            }
        } else {
            res.status(403).send('Lo siento, no tienes permiso para acceder a este contenido.');
        }
    } else {
        res.status(403).send('Token invalido o expirado');
    }
};

module.exports = {
    showProductDB,
    newProduct,
    modProduct,
    delProduct
};